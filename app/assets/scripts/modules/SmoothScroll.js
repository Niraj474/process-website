import $ from 'jquery';
import smoothScroll from 'jquery-smooth-scroll';

class SmoothScroll{
    constructor(){
        this.headerlinks = $(".smooth-scroll a");
        this.addSmoothScrolling();
    }
    addSmoothScrolling(){
        this.headerlinks.smoothScroll();
    }
}
export default SmoothScroll;
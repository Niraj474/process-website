import $ from 'jquery';
import "../styles/style.css";
import MobileMenu from "./modules/MobileMenu";
import RevealOnScroll from "./modules/RevealOnScroll";
import SmoothScroll from "./modules/SmoothScroll";
import ActiveLinks from "./modules/ActiveLinks";
import Modal from "./modules/Modal";
import 'lazysizes';
if (module.hot) {
    module.hot.accept();
}
console.log("App.js is ready!!!!!");

$('.services-box').hover(e => {
    const img = e.currentTarget.querySelector('.services-icon');
    let imgSrc = img.src;
    if (imgSrc.includes('blue'))
        img.src = imgSrc.replace('blue', 'white');
    else
        img.src = imgSrc.replace('white', 'blue');

});

let mobileMenu = new MobileMenu();
new RevealOnScroll($(".section"), "40%");
//new RevealOnScroll($("#departments"), "40%");
new SmoothScroll();
new ActiveLinks();
new Modal();